#include "syscall.h"
void
main()
{
	int n;
	PrintString("Test print string\n");
	PrintString("Hello World!\n");
	PrintString("Test read int\n");
	PrintString("Enter 1 number");
	n = ReadInt();
	PrintString("Test print int\n");
	PrintString("The number is");
	PrintInt(n);
	PrintString("\n");
	PrintString("");
} 
