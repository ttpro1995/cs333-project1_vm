#include "syscall.h"
/* this piece of code can run, however it is not correct - Meow 
We need to use Read and Write function for file in this test case - Meow
void
main()
{
	
	char m[255];
	unsigned int i ;
	OpenFileID inConsole = MyOpenFile("stdin", 2);// input console OpenFileID is 2
	if (inConsole == -1)
	{
		PrintString("Can not open console\n");
		return 0;
	}
	MyReadFile(m, 255, inConsole); 
	PrintString("what you type \n");
	PrintString(m);
	MyCloseFile(inConsole);
}
*/

void
main()
{
	
	char m[255];
	unsigned int i ;
	OpenFileID inConsole = 0; // default file descriptor for stdin is 0
	OpenFileID outConsole = 1; // default file descriptor for stdout is 0

	MyReadFile(m, 255, inConsole); 
	MyWriteFile("echo back \n", 255, outConsole);
	MyWriteFile(m,255,outConsole);
	
}
