#include "syscall.h"
void
main()
{
	
OpenFileID srcId;
	OpenFileID dstId;
	char source[255], dest[255];
	int filesz,srcsz, i;
	char c;
	
	PrintString("Input source file:");
	ReadString(source, 255);
	PrintString("Input destination file:");
	ReadString(dest, 255);
	

	srcId = MyOpenFile(source, 1);
	CreateFile(dest);
	dstId = MyOpenFile(dest, 0);
	if (srcId == -1 || dstId == -1)
	{
		int errorId = srcId == 0 ? 1 : 2;
		PrintString("Can not open file \n");
		PrintString("Terminate program\n");
		return 0;
	}
	filesz = MySeekFile(-1, srcId);
	MySeekFile(0, srcId);
	MySeekFile(0, dstId);
	for (i = 0; i < filesz; ++i)
	{
		MyReadFile(&c, 1, srcId);
		MyWriteFile(&c, 1, dstId);
	}
	MyCloseFile(srcId);
	MyCloseFile(dstId);
	return 0;
} 
