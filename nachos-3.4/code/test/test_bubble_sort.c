#include "syscall.h"

void main()
{
	int n, i, j, tmp;
	int arr[105];
	PrintString("Input number of element (n < 105): ");
	n = ReadInt();
	for (i = 0; i < n; i++) {
		arr[i] = ReadInt();
	} 
	for (i = 0; i < n-1; i++) {
		for (j = i+1; j < n; j++) {
			if (arr[j] < arr[i])
			{
				tmp = arr[j];
				arr[j] = arr[i];
				arr[i] = tmp;
			}
		}
	}
	for (i = 0; i < n; i++) {
		PrintInt(arr[i]);
		PrintChar(' ');
	}


}
