#include "syscall.h"
void
main()
{
	char source[255];
	OpenFileID outConsole = 1; // default file descriptor for stdout is 0
	OpenFileID fID;
	char content[255];
	PrintString("Input source file:");
	ReadString(source, 255);
	fID = MyOpenFile(source, 0);//open file  in code/
	MyReadFile(content,255,fID);//read file content
	PrintString("File content \n");
	MyWriteFile(content, 255, outConsole);
	PrintString("\n End of File content\n");
	MyCloseFile(fID);
} 
